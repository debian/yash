yash (2.55-2) unstable; urgency=medium

  * Add procps to B-D (Closes: 1058530)

 -- TANIGUCHI Takaki <takaki@debian.org>  Thu, 28 Dec 2023 23:46:00 +0900

yash (2.55-1) unstable; urgency=medium

  * New upstream version 2.55
  * debian/doc: follow rename.
  * debian/control: update libdevel dependency.
  * Bump Standards-Version to 4.6.2

 -- TANIGUCHI Takaki <takaki@debian.org>  Tue, 14 Nov 2023 14:02:52 +0900

yash (2.52-2) unstable; urgency=medium

  * Fix FTCBFS (Closes: #1010072)
  * Bump Standards-Version to 4.6.1

 -- TANIGUCHI Takaki <takaki@debian.org>  Sun, 10 Jul 2022 16:04:57 +0900

yash (2.52-1) unstable; urgency=medium

  * New upstream version 2.52
  * Bump Standards-Version to 4.6.0
  * d/rules: fix variable definition

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 01 Apr 2022 18:51:27 +0900

yash (2.51-1) unstable; urgency=medium

  * New upstream version 2.51
  * Bump Standards-Version to 4.5.1
  * Bump debhelper-compat to 13

 -- TANIGUCHI Takaki <takaki@debian.org>  Sat, 26 Dec 2020 17:30:45 +0900

yash (2.50-1) unstable; urgency=medium

  * add CI config
  * New upstream version 2.50
  * Bump Standards-Version to 4.5.0

 -- TANIGUCHI Takaki <takaki@debian.org>  Sun, 07 Jun 2020 11:35:35 +0900

yash (2.49-1) unstable; urgency=medium

  * Use secure URI in Homepage field.
  * New upstream version 2.49
  * Update standards version, no changes needed.
  * Bump debhelper from old 11 to 12.

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 27 Sep 2019 18:22:18 +0900

yash (2.48-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove trailing whitespaces
  * d/rules: Remove trailing whitespaces

  [ TANIGUCHI Takaki ]
  * New upstream version 2.48
  * Bump Stanrads-Version to 4.3.0
  * debian/compat: 11

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 28 Dec 2018 11:24:58 +0900

yash (2.47-1) unstable; urgency=medium

  * New upstream version 2.47
  * Bump Stanrads-Version to 4.1.4

 -- TANIGUCHI Takaki <takaki@debian.org>  Tue, 08 May 2018 15:39:51 +0900

yash (2.46-1) unstable; urgency=medium

  * New upstream version 2.46
  * Move Vcs-* to salsa.debian.org.
  * Bump Standards-Version to 4.1.3
    + debian/control: Remove autotools-dev from B-D.

 -- TANIGUCHI Takaki <takaki@debian.org>  Tue, 02 Jan 2018 16:32:27 +0900

yash (2.45-3) unstable; urgency=medium

  * Add xsltproc, asciidoc, docbook-xsl and asciidoc-common to B-D

 -- TANIGUCHI Takaki <takaki@debian.org>  Mon, 10 Jul 2017 20:38:06 +0900

yash (2.45-2) unstable; urgency=medium

  * debian/control: Change to asciidoc-base in B-D.

 -- TANIGUCHI Takaki <takaki@debian.org>  Mon, 10 Jul 2017 16:05:22 +0900

yash (2.45-1) unstable; urgency=medium

  * New upstream version 2.45
  * debian/control: Add asciidoc to B-D

 -- TANIGUCHI Takaki <takaki@debian.org>  Mon, 10 Jul 2017 14:12:20 +0900

yash (2.44-4) unstable; urgency=medium

  * debian/watch: change to check github releases.
  * Bump Standards-Version to 4.0.0
  * debian/compat: Bump to 10

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 30 Jun 2017 18:40:21 +0900

yash (2.44-3) unstable; urgency=medium

  * debian/control: Add ed to B-D. (Closes: #854199)

 -- TANIGUCHI Takaki <takaki@debian.org>  Mon, 06 Feb 2017 12:39:50 +0900

yash (2.44-2) unstable; urgency=medium

  * debian/rules: Set LANG=C to fix build failures.

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 03 Feb 2017 15:47:54 +0900

yash (2.44-1) unstable; urgency=medium

  * New upstream version 2.44
  * debian/rules: Activate test, again.

 -- TANIGUCHI Takaki <takaki@debian.org>  Thu, 02 Feb 2017 19:47:36 +0900

yash (2.43-1) unstable; urgency=medium

  * Bump Standards-Version to 3.9.8
  * New upstream version 2.43

 -- TANIGUCHI Takaki <takaki@debian.org>  Thu, 20 Oct 2016 17:41:24 +0900

yash (2.39-1) unstable; urgency=medium

  * debian/watch: Change listing page.
  * Imported Upstream version 2.39
    + Fix "fieldsplitting data corruption" (Closes: #786817)

 -- TANIGUCHI Takaki <takaki@debian.org>  Wed, 25 Nov 2015 18:02:13 +0900

yash (2.36-1) unstable; urgency=medium

  * Imported Upstream version 2.36

 -- TANIGUCHI Takaki <takaki@debian.org>  Mon, 23 Jun 2014 13:10:23 +0900

yash (2.35-2) unstable; urgency=medium

  * Remove debian/yash.1. Upstream has a manpage. (Closes: 687658)
  * Bump Standards-Versiont to 3.9.5 (with no changes).

 -- TANIGUCHI Takaki <takaki@debian.org>  Tue, 04 Feb 2014 17:44:33 +0900

yash (2.35-1) unstable; urgency=low

  * debian/rules: Add a hardening build option.
  * Imported Upstream version 2.35
  * Bump Standards-Versiont to 3.9.4 (with no changes).
  * debian/conrol: add versioned dpkg-dev to B-D.

 -- TANIGUCHI Takaki <takaki@debian.org>  Thu, 13 Jun 2013 16:34:02 +0900

yash (2.34-1) unstable; urgency=low

  * Imported Upstream version 2.34
    + Fix "[yash] Error in documentation man." (Closes: #687658)

 -- TANIGUCHI Takaki <takaki@debian.org>  Mon, 06 May 2013 18:13:28 +0900

yash (2.30-2) unstable; urgency=low

  * Add yash.1 (by Khalid El Fathi) (Closes: #673246)

 -- TANIGUCHI Takaki <takaki@debian.org>  Fri, 18 May 2012 20:14:29 +0900

yash (2.30-1) unstable; urgency=low

  * Imported Upstream version 2.30
  * Update debian/copyright format as in Debian Policy 3.9.3

 -- TANIGUCHI Takaki <takaki@debian.org>  Sun, 26 Feb 2012 18:49:27 +0900

yash (2.29-1) unstable; urgency=low

  * New upstream (Closes: #650023)
  * debian/watch: Fix match pattern.
  * debian/control: Add Vcs-* fields.
  * Bump Standards-Version to 3.9.2.

 -- TANIGUCHI Takaki <takaki@debian.org>  Thu, 01 Dec 2011 11:04:21 +0900

yash (2.25-1) unstable; urgency=low

  * Initial release (Closes: #609307)

 -- TANIGUCHI Takaki <takaki@debian.org>  Wed, 19 Jan 2011 00:33:11 +0900
